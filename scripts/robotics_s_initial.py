from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


#this creates the class
class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""
    
    #this initializes the ROS node and uses the MoveGroupCommander to control the robot
    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")


        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self, joint_vars):
       
        move_group = self.move_group


        #this breaks down the list of joint parameters passed in through calling the function so that the appropriate value goes to the appropriate joint
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = (joint_vars[0] * pi / 180)
        joint_goal[1] = (joint_vars[1]* pi / 180)
        joint_goal[2] = (joint_vars[2]* pi / 180)
        joint_goal[3] = (joint_vars[3]* pi / 180)
        joint_goal[4] = (joint_vars[4]* pi / 180)
        joint_goal[5] = (joint_vars[5]* pi / 180)

        
        move_group.go(joint_goal, wait=True)

       
        move_group.stop()

#this initializes an instance of the class
move_group_interface = MoveGroupPythonInterfaceTutorial()

#this lists out the coordinates of the joints used for my initials in radians, converted from degrees

start_pos = [3, -79, 7, -98, -7, 170]
s_one = [4, -119, 91, -143, -8, 172]
s_two = [0, -90, 51, -124, -4, 164]
s_three = [-1, -78, 53, -130, -3, 155]
s_four = [2, -123, 113, -159, -6, 169]
s_five = [-1, -99, 120, -176, -3, 155]
s_six = [-2, -74, 78, -150, -2, 146]


# this uses the class instance to call the function and passes in the appropriate list
print("going to start position")
move_group_interface.go_to_joint_state(start_pos)
print("starting S initial")
move_group_interface.go_to_joint_state(s_one)
move_group_interface.go_to_joint_state(s_two)
move_group_interface.go_to_joint_state(s_three)
move_group_interface.go_to_joint_state(s_four)
move_group_interface.go_to_joint_state(s_five)
move_group_interface.go_to_joint_state(s_six)
print("finished S initial")
print("returning to starting point")
move_group_interface.go_to_joint_state(start_pos)
print("starting J initial")

