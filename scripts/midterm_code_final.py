#importing necessary packages for movement
import sys
import rospy
import moveit_commander
import moveit_msgs.msg
from math import pi

#this is the creation of the class that will allow for interaction between the python code and the simulation
class MoveGroupPythonInterfaceTutorial(object):
    
    #this function initializes the ROS node and the MoveGroupCommander to control the robot
    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        #initialize moveit_commander and rospy here
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        #initialize an object that interfaces with the robot
        robot = moveit_commander.RobotCommander()

        #initialize an object that interacts with the scene around the robot
        scene = moveit_commander.PlanningSceneInterface()

        #initialize an object that interacts with the manipulator group of joints
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # create move_group attribute
        self.move_group = move_group
        self.start_pos = [3, -79, 7, -98, -7, 170]
    #this is a function that takes in a list of joint positions, assigns positions to corresponding joints, and tells the simulation to move and then stop when the desired position is reached 
    def go_to_joint_state(self, joint_vars):
        
        #accessing object's move_group attribute 
        move_group = self.move_group
        
        # gets the existing joint values so the state of the robot can be changed 
        joint_goal = move_group.get_current_joint_values()

        #this breaks down the list of joint parameters passed in when calling the function 
        #the input degree values are also converted to radians before being assigned to the appropriate joint
        joint_goal[0] = (joint_vars[0] * pi / 180)
        joint_goal[1] = (joint_vars[1]* pi / 180)
        joint_goal[2] = (joint_vars[2]* pi / 180)
        joint_goal[3] = (joint_vars[3]* pi / 180)
        joint_goal[4] = (joint_vars[4]* pi / 180)
        joint_goal[5] = (joint_vars[5]* pi / 180)

        #this moves the joints to the desired position after being assigned
        move_group.go(joint_goal, wait=True)

       #this stops movement 
        move_group.stop()


    # draw the S initial
    def draw_s(self):
        #declare each joint position
        start_pos = self.start_pos
        s_one = [4, -119, 91, -143, -8, 172]
        s_two = [0, -90, 51, -124, -4, 164]
        s_three = [-1, -78, 53, -130, -3, 155]
        s_four = [2, -123, 113, -159, -6, 169]
        s_five = [-1, -99, 120, -176, -3, 155]
        s_six = [-2, -74, 78, -150, -2, 146]

        # call go_to_joint_state to move the robot 
        print("going to start position")
        self.go_to_joint_state(start_pos)
        print("starting S initial")
        self.go_to_joint_state(s_one)
        print("S in progress")
        self.go_to_joint_state(s_two)
        self.go_to_joint_state(s_three)
        self.go_to_joint_state(s_four)
        self.go_to_joint_state(s_five)
        self.go_to_joint_state(s_six)
        print("finished S initial")

    # draw the J initial
    def draw_j(self):
        #declare each joint position
        start_pos = self.start_pos
        j_one = [2, -123, 113, -159, -6, 169]
        j_two = [-1, -99, 120, -176, -3, 155]
        j_three = [-2, -74, 78, -150, -2, 146]

        # call go_to_joint_state to move the robot 
        print("returning to starting point")
        move_group_interface.go_to_joint_state(start_pos)
        print("starting J initial")
        print("J in progress")
        move_group_interface.go_to_joint_state(j_one)
        move_group_interface.go_to_joint_state(j_two)
        move_group_interface.go_to_joint_state(j_three)
        print("finished J initial")

    # draw the H initial
    def draw_h(self):
        #declare each joint position
        start_pos = self.start_pos
        h_start = [-1, -94, 69, -137, -3, 160]
        h_one = [-1, -100, 95, -154, -3, 158]
        h_two = [-2, -94, 126, -184, -2, 152]
        h_three = [-2, -69, 64, -141, -2, 145]
        h_four = [-2, -57, 18, -113, -2, 150]
        h_five = [-3, -66, 90, -164, -1, 138]

        # call go_to_joint_state to move the robot 
        print("returning to starting point")
        move_group_interface.go_to_joint_state(start_pos)
        print("starting H initial")
        move_group_interface.go_to_joint_state(h_start)
        print("H in progress")
        move_group_interface.go_to_joint_state(h_one)
        move_group_interface.go_to_joint_state(h_two)
        move_group_interface.go_to_joint_state(h_one)
        move_group_interface.go_to_joint_state(h_three)
        move_group_interface.go_to_joint_state(h_four)
        move_group_interface.go_to_joint_state(h_three)
        move_group_interface.go_to_joint_state(h_five)
        print("finished H initial")

#this initializes an instance of the class so that it can be manipulated
move_group_interface = MoveGroupPythonInterfaceTutorial()

#call each function to draw the letter
move_group_interface.draw_s()
move_group_interface.draw_j()
move_group_interface.draw_h()







